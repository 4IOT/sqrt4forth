\ 250000
: .sqrtPileUp ( n^2 -- n1 n2 ... nx  )
  begin
    dup 3 and swap 4 /
    (  ... n -- -1 ... xx n/4 )
    dup 4 <
  until
  (  ... xx n --  ... xx 0...0XX )
  \  0 0 1 2 0 0 1 3 3
;
  
: .sqrtCompute ( -1 ... xx 0...0XX -- -n )
  0 >r \ store initial Q0
  begin
    \ dup q @ 4 * 1 + - dup 0 < dup
    dup r@ 4 * 1 + - dup 0 < dup
    \ 1 + q @ 2 * + q !
    1 + r> 2 * + >r
    0>= If
      swap drop
    else
      drop
    then
    4 * +
    over 0< 
    cr ." Loop"
  until
  ( -1 -- )
  drop  drop
  ( -- sqrt)
  r> 
;
  
: sqrt ( n^2 -- n )
  -1 \ botom marker
  swap
  0 \ botom marker
  swap
  .sqrtPileUp  ( -1 n2 -- -1 ... xx 0...0XX )
  \ -1 0 0 1 2 0 0 1 3 3
  .sqrtCompute  ( -1 ... xx 0...0XX -- sqrt)
;
