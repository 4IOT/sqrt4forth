# .sqrtCompute

prepare :

```forth
variable q                     
0 q !
169 -1 swap 
2 base ! .s
```
    <10> -1 10101001  ok

```forth
.sqrtPileUp cr .s       
```
returns :

    <10> -1 01 10 10 10  ok

step by step :

```forth
dup q @ 100 * 1 + - dup 0 < dup cr .s
```

    <1000> -1 1 10 10 10 1 0 0  ok

```forth
1 + q @ 10 * + q ! 
drop q @ cr ." q=" .  cr .s cr

```

    q=1
    <110> -1 1 10 10 10 1  ok

```forth
swap drop
```

    q=1
    <101> -1 1 10 10 1  ok

```forth
100 * +  cr .s
```

    q=1
    <100> -1 1 10 110  ok

```forth
dup 0>=  cr .s
```

    q=1
    <101> -1 1 10 110 -1  ok


Loop 

```forth
drop \ until

```

    q=1
    <100> -1 1 10 110  ok

```forth
dup q @ 100 * 1 + - dup 0 < dup cr .s
```

    q=1
    <111> -1 1 10 110 1 0 0  ok

```forth
1 + q @ 10 * + q ! drop cr .s             
```

    q=11
    <101> -1 1 10 110 1  ok

```forth
swap drop
```

    q=11
    <100> -1 1 10 1  ok

```forth
100 * +  cr .s
```

    q=11
    <11> -1 1 110  ok

```forth
dup 0>=  cr .s
```

    q=11
    <100> -1 1 110 -1  ok

Loop 

```forth
drop  cr .s \ until

```

    <11> -1 1 110  ok

```forth
dup q @ 100 * 1 + - dup 0 < dup cr .s
```

    q=11
    <110> -1 1 110 -111 -1 -1  ok

```forth
1 + q @ 10 * + q ! 
drop q @ cr ." q=" .  cr .s cr

```

    q=110
    <100> -1 1 110 -111  ok

Negativ

```forth
drop
```

    q=110
    <101> -1 1 10 10 1  ok

```forth
100 * +  cr .s
```

    q=110
    <10> -1 11001  ok

```forth
dup 0>=  cr .s
```

    q=110
    <11> -1 11001 0  ok

Loop 

```forth
drop \ until

```

    q=110
    <10> -1 11001  ok

```forth
dup q @ 100 * 1 + - dup 0 < dup cr .s
```

    q=110
    <101> -1 11001 0 0 0  ok

```forth
1 + q @ 10 * + q ! 
drop q @ cr ." q=" .  cr .s cr

```

    q=1101 
    <11> -1 11001 0 

```forth
swap drop
```

    q=1101 
    <10> -1 0  ok

```forth
100 * +  cr .s
```

    q=1101 
    <1> -1  ok

```forth
dup 0>=  cr .s
```

    <10> -1 0  ok

End

```forth
drop 
drop 
q @ decimal cr .s  
```
